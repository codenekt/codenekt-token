from util import wait_for_confirmation, print_created_asset, print_asset_holding
from algosdk.future import transaction
from algosdk import mnemonic
from algosdk.v2client import algod
import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())


# Setup Algorand Account
signer_mnemonic = os.getenv('mnemonic')
public_key = mnemonic.to_public_key(signer_mnemonic)
secret_key = mnemonic.to_private_key(signer_mnemonic)

print("Account Address: {}".format(public_key))

# Initialize an algod client
algod_address = os.getenv('algod_address')
algod_token = os.getenv('algod_token')
algod_client = algod.AlgodClient(
    algod_token=algod_token, algod_address=algod_address)


# ASSET CONFIGURATION

# IMMUTABLE ASSET CONFIGURATION
ASSET_NAME = "CDK Token"
UNIT_NAME = "CDK"
DECIMALS = 4 # The number of digits to use after the decimal point when displaying the asset.
TOTAL_SUPPLY = 30000000000 # The total number of base units of the asset to create. This number cannot be changed.
URL = "https://cdktoken.io" # Specifies a URL where more information about the asset can be retrieved. Max size is 96 bytes.

# MUTABLE ASSET CONFIGURATION
MANAGER = "DODTPZG56R6IBU22XKM73FMB5TO63POKQ4TWWWON4DKW656XJDFSEBDYAI" # The manager account is the only account that can authorize transactions to re-configure or destroy an asset.
RESERVE = "DODTPZG56R6IBU22XKM73FMB5TO63POKQ4TWWWON4DKW656XJDFSEBDYAI" # Specifying a reserve account signifies that non-minted assets will reside in that account instead of the default creator account. 
FREEZE = "DODTPZG56R6IBU22XKM73FMB5TO63POKQ4TWWWON4DKW656XJDFSEBDYAI" # The freeze account is allowed to freeze or unfreeze the asset holdings for a specific account. When an account is frozen it cannot send or receive the frozen asset.
CLAWBACK = "DODTPZG56R6IBU22XKM73FMB5TO63POKQ4TWWWON4DKW656XJDFSEBDYAI" # The clawback address represents an account that is allowed to transfer assets from and to any asset holder (assuming they have opted-in). 


def create_asset(
    sender,
    sender_sk,
    total,
    unit_name,
    asset_name,
    url,
    manager,
    reserve,
    freeze,
    clawback,
    decimals
):
    params = algod_client.suggested_params()
    params.fee = 1000
    params.flat_fee = True

    # Asset Creation transaction
    asset_creation_transaction = transaction.AssetConfigTxn(
        sender=sender,
        sp=params,
        total=total,
        default_frozen=False,
        unit_name=unit_name,
        asset_name=asset_name,
        manager=manager,
        reserve=reserve,
        freeze=freeze,
        clawback=clawback,
        url=url,
        decimals=decimals)

    signed_asset_creation_transaction = asset_creation_transaction.sign(
        sender_sk)

    # Send the transaction to the network and retrieve the txid.
    asset_creation_transaction_id = algod_client.send_transaction(
        signed_asset_creation_transaction)
    print("Asset Creation Transaction Id = ", asset_creation_transaction_id)

    # Wait for the transaction to be confirmed
    wait_for_confirmation(algod_client, asset_creation_transaction_id)

    try:
        ptx = algod_client.pending_transaction_info(
            asset_creation_transaction_id)
        asset_id = ptx["asset-index"]
        print_created_asset(algod_client, sender, asset_id)
        print_asset_holding(algod_client, sender, asset_id)
        return asset_id
    except Exception as e:
        print(e)


# CREATE ASA
print("Creating ASA")
CDK_id = create_asset(
    sender=public_key,
    sender_sk=secret_key,
    total=TOTAL_SUPPLY,
    unit_name=UNIT_NAME,
    asset_name=ASSET_NAME,
    url=URL,
    manager=MANAGER,
    reserve=RESERVE,
    freeze=FREEZE,
    clawback=CLAWBACK,
    decimals=DECIMALS)
