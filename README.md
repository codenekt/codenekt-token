# Algorand ASA Creation

This repo contains a script to deploy an ASA on the Algorand blockchain.

## Usage

The file `deploy_ASA.py` contains the script to deploy ASA. Modify the Fields under ASSET CONFIGURATION to customize ASA.

## Development Setup

This repo requires Python 3.6 or higher. We recommend you use a Python virtual environment to install
the required dependencies.

Set up venv (one time):
 * `python3 -m venv venv`

Active venv:
 * `. venv/bin/activate` (if your shell is bash/zsh)
 * `. venv/bin/activate.fish` (if your shell is fish)

Install dependencies:
* `pip install -r requirements.txt`

Format code:
* `black .`